/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;
//import java.util.Calendar;
import java.util.Date;
//import java.util.HashSet;
//import javax.xml.datatype.Duration;
import pk.labs.Lab9.beans.Term;
/**
 *
 * @author terg
 */
public class Termin implements Term {
    
    private Date rozpoczecie;
    
    private int czas_trwania;
    
    public Termin(){}
    

    @Override
    public int getDuration() {
        return  this.czas_trwania;
    }
    @Override
    public void setDuration(int duration) {
        if(duration > 0){       
            this.czas_trwania = duration; 
        }
        else {
            this.czas_trwania = 30;
        }       
    }
    @Override
    public Date getBegin() {
        return this.rozpoczecie;
    }

    @Override
    public void setBegin(Date begin) {
        this.rozpoczecie = begin;              
    }

    @Override
    public Date getEnd() {
        Date koniec=new Date(this.rozpoczecie.getTime() + this.czas_trwania * 60000);
        return koniec;
        
    }   
}
