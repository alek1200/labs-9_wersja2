/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
//import java.util.Date;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.Term;
/**
 *
 * @author terg
 */
public class Listener implements VetoableChangeListener  {
   
    private ConsultationList lista_konsultacji;
    public Listener(ConsultationList lista)
    {
        this.lista_konsultacji = lista;
    }
    
    @Override
    public void vetoableChange(PropertyChangeEvent e) throws PropertyVetoException {       
        Term stary = (Term)e.getOldValue();
        Term nowy = (Term)e.getNewValue();
        for(int i=0; i < this.lista_konsultacji.getSize();i++)
        {           
            if(nowy.getEnd().after(this.lista_konsultacji.getConsultation(i).getBeginDate()) )
            {
                if (!stary.getBegin().equals(this.lista_konsultacji.getConsultation(i).getBeginDate())) {
                    if (nowy.getBegin().before(this.lista_konsultacji.getConsultation(i).getEndDate())) {
                       throw new PropertyVetoException("blad!", null); 
                    }
                    
                }
                
            }
        }
    }
}
