/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;
import java.util.Date;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;
/**
 *
 * @author terg
 */
public class Konsultacja implements Consultation{
    
    private final VetoableChangeSupport veto = new VetoableChangeSupport(this);
    private final PropertyChangeSupport prop = new PropertyChangeSupport(this);
    
    private String student;
    private Term termin;
    private Date end;
    
    public Konsultacja(){}
    
    @Override
    public String getStudent() {
        return this.student;
    }

    @Override
    public void setStudent(String student) {
        this.student = student;
    }

    @Override
    public Date getBeginDate() {
        return this.termin.getBegin();
    }

    @Override
    public Date getEndDate() {
        return this.termin.getEnd();
    }
    public Term getTerm()
    {
        return this.termin;
    }
    @Override
    public void setTerm(Term term) throws PropertyVetoException {
        Term stary_termin = this.termin;
        veto.fireVetoableChange("termin", stary_termin, term);
        this.termin = term;
        prop.firePropertyChange("termin", stary_termin, term);
        
    }
    
    @Override
    public void prolong(int minutes) throws PropertyVetoException {    
        if(minutes <= 0) minutes=0;  
        Term stary_termin = this.termin;
        Term nowy_termin = new Termin();
        nowy_termin.setBegin(stary_termin.getBegin());
        if (nowy_termin.getBegin()==stary_termin.getBegin()) {
            nowy_termin.setDuration(minutes + stary_termin.getDuration());
            this.veto.fireVetoableChange("termin", stary_termin, nowy_termin);
            this.termin = nowy_termin;
            this.prop.firePropertyChange("termin", stary_termin, nowy_termin);
        }

    }
    
    public void dodVCS(Listener listener){
        this.veto.addVetoableChangeListener(listener);
    }
}
