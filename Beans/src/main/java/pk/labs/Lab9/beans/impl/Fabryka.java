/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;
import java.beans.PropertyVetoException;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.io.FileInputStream;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;
/**
 *
 * @author terg
 */
public class Fabryka implements ConsultationListFactory {
  
    @Override
    public ConsultationList create() {
        return new ListaKonsultacji();
    }

    @Override
    public ConsultationList create(boolean deserialize) {
        Consultation[] wynik = null;
        ConsultationList lista = new ListaKonsultacji();
        if(deserialize)
        {
        try{
            XMLDecoder decoder = new XMLDecoder( new BufferedInputStream( new FileInputStream("save.xml")));
            
            wynik = (Consultation[])decoder.readObject();
            decoder.close();
        if(wynik != null)
        {
        for(int i=0; i < wynik.length ; i++ )
        {
            try 
            {
                lista.addConsultation(wynik[i]);
            } 
            catch (PropertyVetoException e) 
            {
                e.getMessage();
            }
        }
        }
            }
        catch(FileNotFoundException e) 
        {
            e.getMessage();
        }                
      
        }
        else create();
        return lista;
        
    }

    @Override
    public void save(ConsultationList consultationList) {
        
        try
        {
            XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("save.xml")));

            encoder.writeObject(consultationList.getConsultation());
            //encoder.flush();
            encoder.close();
        }
        catch(FileNotFoundException e)
        {
            e.getMessage();
        }
        }
}
