/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.util.Arrays;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.impl.Listener;
/**
 *
 * @author terg
 */
public class ListaKonsultacji implements ConsultationList {
 
    private final Listener sluchacz = new Listener(this);
    private final PropertyChangeSupport prop = new PropertyChangeSupport(this);
    private final VetoableChangeSupport veto = new VetoableChangeSupport(this);
    
    private Consultation[] lista;

    public ListaKonsultacji()
    {
        this.lista = new Consultation[]{};
    }
    
    /*public void setSize(int rozmiar){
    
    }*/
    @Override
    public int getSize() {
        return this.lista.length;
    }

    @Override
    public Consultation[] getConsultation() {
        return this.lista;
    }

    @Override
    public Consultation getConsultation(int index) {
        return this.lista[index];
    }

    @Override
    public void addConsultation(Consultation consultation) throws PropertyVetoException {       
        
        for(int i=0; i < this.lista.length;i++)
        {
            if((consultation.getBeginDate().before(this.lista[i].getEndDate()) && consultation.getEndDate().after(this.lista[i].getBeginDate()))||(consultation.getEndDate().after(this.lista[i].getBeginDate()) && consultation.getBeginDate().before(this.lista[i].getBeginDate())))
            {
                throw new PropertyVetoException("Blad", null);
            }          
        }   
        
        ((Konsultacja)consultation).dodVCS(sluchacz);
        Consultation[] nowa = Arrays.copyOf(this.lista,  this.lista.length + 1);
        Consultation[] stara = this.lista;
        nowa[nowa.length - 1] = consultation;
        if (nowa.getClass()==stara.getClass()) {
                this.lista = nowa;       
                prop.firePropertyChange("consultation", stara, nowa);
        }

       
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
       this.prop.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.prop.addPropertyChangeListener(listener);
    }
    
    public void addVetoableChangeListener(VetoableChangeListener listener) {
        this.veto.addVetoableChangeListener(listener);
    }

    public void removeVetoableChangeListener(VetoableChangeListener listener) {
        this.veto.removeVetoableChangeListener(listener);
    }
}
